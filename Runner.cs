﻿using UnityEngine;
using System.Collections;

public class Runner : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameEventManager.GameStart += GameStart;
		GameEventManager.GameOver += GameOver;
		startPosition = transform.localPosition;
		renderer.enabled = false;
		rigidbody.isKinematic = true;
		enabled = false;
	}

	private void GameStart () {
		boosts = 0;
		distanceTraveled = 0f;
		GUIManager.SetDistance(distanceTraveled);
		transform.localPosition = startPosition;
		renderer.enabled = true;
		rigidbody.isKinematic = false;
		enabled = true;
	}
	
	private void GameOver () {
		renderer.enabled = false;
		rigidbody.isKinematic = true;
		enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Jump")){
			if(touchingPlatform){
				rigidbody.AddForce(jumpVelocity, ForceMode.VelocityChange);
				touchingPlatform = false;
			}
			else if(boosts > 0){
				rigidbody.AddForce(boostVelocity, ForceMode.VelocityChange);
				boosts -= 1;
			}
		}
		//
		else if(Input.GetButtonDown("Slow")){
			if(touchingPlatform){
				this.jumpVelocity.x = this.jumpVelocity.x - 0.5f * this.jumpVelocity.x;
				this.jumpVelocity.y = this.jumpVelocity.y - 0.5f * this.jumpVelocity.y;

				rigidbody.AddForce(jumpVelocity, ForceMode.VelocityChange);
				touchingPlatform = false;
			}
			else if(boosts > 0){
				this.boostVelocity.x = this.boostVelocity.x - 0.3f * this.boostVelocity.x;
				this.boostVelocity.y = this.boostVelocity.y - 0.3f * this.boostVelocity.y;

				rigidbody.AddForce(boostVelocity, ForceMode.VelocityChange);
			}
		}
		else if(Input.GetButtonDown("Slow2")){
			if(touchingPlatform){
				this.jumpVelocity.x = this.jumpVelocity.x - 0.5f * this.jumpVelocity.x;
				this.jumpVelocity.y = this.jumpVelocity.y - 0.5f * this.jumpVelocity.y;
				
				rigidbody.AddForce(jumpVelocity, ForceMode.VelocityChange);
				touchingPlatform = false;
			}
			else if(boosts > 0){
				this.boostVelocity.x = this.boostVelocity.x - 0.3f * this.boostVelocity.x;
				this.boostVelocity.y = this.boostVelocity.y - 0.3f * this.boostVelocity.y;
				
				rigidbody.AddForce(boostVelocity, ForceMode.VelocityChange);
			}
		}
		else if(Input.GetButtonDown("Follow")){
			if(touchingPlatform){
				this.jumpVelocity.x = this.jumpVelocity.x + 0.5f * this.jumpVelocity.x;
				this.jumpVelocity.y = this.jumpVelocity.y + 0.5f * this.jumpVelocity.y;
				
				rigidbody.AddForce(jumpVelocity, ForceMode.VelocityChange);
				touchingPlatform = false;
			}
			else if(boosts > 0){
				this.boostVelocity.x = this.boostVelocity.x + 0.3f * this.boostVelocity.x;
				this.boostVelocity.y = this.boostVelocity.y + 0.3f * this.boostVelocity.y;
				
				rigidbody.AddForce(boostVelocity, ForceMode.VelocityChange);
			}
		}
		//
		distanceTraveled = transform.localPosition.x;
		GUIManager.SetDistance(distanceTraveled);

		if(transform.localPosition.y < gameOverY){
			GameEventManager.TriggerGameOver();
		}
	}

	void FixedUpdate () {
		if(touchingPlatform){
			rigidbody.AddForce(acceleration, 0f, 0f, ForceMode.Acceleration);
		}
	}
	
	void OnCollisionEnter () {
		touchingPlatform = true;
	}
	
	void OnCollisionExit () {
		touchingPlatform = false;
	}

	public static void AddBoost () {
		boosts += 1;
	}
	
	public static float distanceTraveled;
	public float acceleration;
	public Vector3 jumpVelocity, boostVelocity;
	public float gameOverY;

	private bool touchingPlatform;
	private Vector3 startPosition;
	private static int boosts;

}
